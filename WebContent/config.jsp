<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.orsyp.tools.uvmsmonitor.*"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ORSYP UVMS Monitor</title>
<link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<%			
	Configuration conf = ConfigFactory.getConfig();  
%>					
<body>
	<div class="titletablepanel">
	<table width="100%">
		<tr>
			<td width="100px"><img src="img/logo.png" /></td>
			<td class="title">ORSYP UVMS Monitor Configuration <small style="font-size: 10px;">(v 1.2)</small></td>
			<td style="font-size:12px" width="70px"><a href="index.jsp"  title="Back to main page"><img src="img/home.png"/><br>Home</a></td>
		</tr>
	</table>
	</div>
	<div class="tablepanel">
			<form id="refreshForm" name="refreshForm" method="post" action="SaveRefreshServlet" align="right">
			Refresh interval (in seconds)
			<input type="text" name="refresh" id="f_refresh" style="width:40px" value="<%=conf.refreshInterval%>" />
			<input type="submit" class="button" id="btsubmitrefresh" name="submit" onsubmit="this.disabled=true;" value="Save" />
			</form>
	</div>
	<div class="tablepanel">
		<form id="myForm" name="form" method="post" action="AddUVMSServlet" style="padding-bottom:10px">
		<fieldset>
			<legend id="boxtitle"> Add new UVMS </legend>
			<table>
				<tr>
					<td>UVMS address</td>
					<td><input type="text" name="name" id="f_name" style="width:150px"/> <small>Ex: server1:1234. Default port 4184 can be omitted</small></td>
				</tr>
				<tr>
					<td>User</td>
					<td><input type="text" name="user" id="f_user" style="width:150px"/></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><input type="password" name="pwd" id="f_pwd" style="width:150px"/></td>
				</tr>
				<tr>
					<td>Web url</td>
					<td><input type="text" name="web" id="f_web" style="width:400px"/></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" class="button" id="btsubmit" name="submit" onsubmit="this.disabled=true;" value="" />
						<input type="button" class="button" value="Cancel" onclick="showbox(true);">	
					</td>
				</tr>
			</table>	
		</fieldset>		
		</form>
	
	<table id="mytable">
		<tr>			    	
		<th width="180px">UVMS address</th>		
		<th width="150px">User</th>		
		<th>Web url</th>
		<th width="22px"></th>
		</tr>

<%			
	for (UVMS u : conf.uvmsList) {
%>					
		<tr data-tt-id="3" data-tt-parent-id="2">		           
	        <td><%=u.hostname%></td>
			<td><%=u.user%></td>
	        <td><%=u.webUrl%></td>
	        <td>
	        	<a href="#" title="Modify configuration" onClick="editRow('<%=u.hostname%>','<%=u.user%>','<%=u.webUrl%>');"><img src="img/edit.png" alt="Mod"/></a>
		        <a href="DeleteUVMSServlet?name=<%=u.hostname%>" title="Delete"><img src="img/delete.png" alt="Canc"/></a>
		    </td>		
		</tr>
<%	} %>		
	</table>
	</div>	
</body>

<script type="text/javascript">	
	window.onload = function(){	
		showbox(true);
	};

	function showbox(erase) {
		if (erase) {
			document.getElementById('btsubmit').value = 'Add';
			document.getElementById('boxtitle').innerHTML = 'Add new UVMS';				
			document.getElementById('f_name').value='';
			document.getElementById('f_pwd').value='';
			document.getElementById('f_user').value='';
			document.getElementById('f_web').value='';
		} else {
			document.getElementById('btsubmit').value = 'Modify';
			document.getElementById('boxtitle').innerHTML = 'Modify UVMS';			
		}
		document.getElementById('btsubmit').disabled=false;
	};

	function editRow(name,user,web) {
		document.getElementById('f_name').value=name;
		document.getElementById('f_pwd').value='';
		document.getElementById('f_user').value=user;
		document.getElementById('f_web').value=web;
		showbox(false);	
		scroll(0,0);	
	}

	function stopRKey(evt) {
		var evt = (evt) ? evt : ((event) ? event : null);
		var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
		if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
	}

	document.onkeypress = stopRKey; 	
</script>
</html>