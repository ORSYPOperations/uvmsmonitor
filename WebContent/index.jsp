<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	import="com.orsyp.tools.uvmsmonitor.*"
    pageEncoding="ISO-8859-1"%>
<%			
	Configuration conf = ConfigFactory.getConfig();
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta http-equiv="refresh" content="<%=conf.refreshInterval%>" >
<title>ORSYP UVMS Monitor</title>
<link rel="stylesheet" href="css/style.css" type="text/css">
<link href="css/jquery.treetable.css" rel="stylesheet" type="text/css" />
<link href="css/jquery.treetable.theme.css" rel="stylesheet" type="text/css" />
<script src="js/jquery-1.9.1.min.js"></script>
<script src="js/jquery.treetable.js"></script>
</head>
<body>
	<div class="titletablepanel">
	<table width="100%">
		<tr>
		<td width="100px"><img src="img/logo.png" /></td>
		<td class="title">ORSYP UVMS Monitor</td>
		<td style="font-size:12px" width="70px"><a href="config.jsp"  title="Configure UVMS nodes and refresh time"><img src="img/settings.png"/><br>Configure</a></td>
		</tr>
	</table>
	</div>
	<div class="tablepanel">
	<table id="mytable">
		<tr>			    	
		<th >UVMS/Node</th>		
		<th width="450px">Job chain</th>
		<th width="170px">Status</th>
		</tr>
<%			
	int idx=0;
	for (UVMS u : conf.uvmsList) {
		int uvmsIdx=idx++;
		
		if (idx>1) {
%>					
		<tr>		           
	        <td colspan="3" class="separator">&nbsp;</td>
		</tr>			
<%			
		}
		
		int span=3;
		if (u.connectionError || u.nodes==null)
			span=1;
			
%>			
		<tr data-tt-id="<%=uvmsIdx%>">		           
	        <td colspan="<%=span%>">
	        	<img class="uvms" src="img/server.png" />
<%			
		if (u.webUrl==null || u.webUrl.length()==0) {
%>
				<%=u.hostname%>
<%		
		}
		else {
%>	        	
	        	<a href="<%=u.webUrl%>" target="_blank" title="Connect to <%=u.hostname%> web console"><%=u.hostname%></a>
<%			
		}
%>
		</td>
<%
		if (u.connectionError) {
%>	        	
        	<td  colspan="2" class="warning">Connection error</td>
<%						
		}
		else
		if (u.nodes==null){
%>	        	
        	<td  colspan="2" class="empty">Reading nodes ...</td>
<%						
		}			
%>	        	
		</tr>
<%	
		if (u.nodes!=null)
			for (DuNode n : u.nodes) {
				int nodeIdx=idx++;
				
				if (!n.connected) {
%>				
		<tr data-tt-id="<%=nodeIdx%>" data-tt-parent-id="<%=uvmsIdx%>">		           
	        <td title="Node: <%=n.name%> Company: <%=n.company%>"><img class="node" src="img/nodes.png" /><%=n.name%></td>
	        <td  colspan="2" class="warning">Disconnected</td>
		</tr>		
<%					
				
				}
				else
				if (n.jobChains.size()==0) {
%>				
		<tr data-tt-id="<%=nodeIdx%>" data-tt-parent-id="<%=uvmsIdx%>">		           
	        <td title="Node: <%=n.name%> Company: <%=n.company%>"><img class="node" src="img/nodes.png" /><%=n.name%></td>
	        <td  colspan="2" class="empty">No executions</td>
		</tr>		
<%					
				}
				else {
%>				
		<tr data-tt-id="<%=nodeIdx%>" data-tt-parent-id="<%=uvmsIdx%>">		           
	        <td colspan="3"><img class="node" src="img/nodes.png" /><%=n.name%></td>
		</tr>		
<%	
			
					for (JobChain j : n.jobChains) {
						int jcIdx=idx++;
%>				
		<tr data-tt-id="<%=jcIdx%>" data-tt-parent-id="<%=nodeIdx%>">
	        <td></td>
			<td><%=j.name%></td>
	        <td class="status" style="background-color:<%=j.color%>;"><%=j.status%></td>
		</tr>		
<%	
					}
				}
			}
	}
%>				
	</table>
	</div>
</body>
<script>
$("#mytable").treetable({ expandable: true, initialState: "expanded" });
</script>
</html>