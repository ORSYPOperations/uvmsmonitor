package com.orsyp.tools.uvmsmonitor;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;


public class DataReaderTask extends TimerTask {
		
	public void run() {
		Configuration c = ConfigFactory.getConfig();	
		try {
			for (UVMS u : c.uvmsList) {
				System.out.println("Reading "+u.hostname+"....");
				u.loadData();
				System.out.println("Done "+u.hostname);
			}
		} catch (Exception e) {
			System.out.println("Error reading data");
		}
		
		Timer timer = new Timer();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, c.refreshInterval);
		timer.schedule(new DataReaderTask(), cal.getTime());
	}

}
