package com.orsyp.tools.uvmsmonitor;

import java.util.Calendar;
import java.util.Timer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

public class DataReaderInitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void init () throws ServletException {
		Timer timer = new Timer();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, 10);
		timer.schedule(new DataReaderTask(), cal.getTime());
	}

}
