package com.orsyp.tools.uvmsmonitor;

import java.util.ArrayList;

public class DuNode {
	public String name;
	public String company;
	public boolean connected = false;
	public ArrayList<JobChain> jobChains = new ArrayList<JobChain>();
}
