package com.orsyp.tools.uvmsmonitor;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;

public class ConfigFactory {
	
	private static Configuration config = null;
	
	public static Configuration getConfig() {
		if (config==null)
			config=loadConfiguration();
		return config;
	}

	private static Configuration loadConfiguration() {
		Configuration conf = null;
		try {
			// use buffering
			InputStream buffer = new BufferedInputStream(new FileInputStream(Configuration.SERIALIZED_FILE));
			ObjectInput input = new ObjectInputStream(buffer);
			try {
				// deserialize the List
				conf = (Configuration) input.readObject();
			} finally {
				input.close();
			}
		} catch (Exception ex) {
			System.out.println("Error loading UVMS list file");
			conf = new Configuration();
		}
		
		return conf;
	}
	
		/*
		//create test data 
		
		conf = new Configuration();
		
		UVMS u = new UVMS();
		u.hostname = "TEST_SERVER1";
		u.user = "testuser";
		u.password = "pwd";
		u.webUrl = "http://testserver1.domain.com/web_console";
		conf.uvmsList.add(u);
		
		DuNode n = new DuNode();
		n.name = "NODE1A";
		u.nodes.add(n);
		
		JobChain j = new JobChain();
		j.name = "JOBCHAIN_1A_01";
		j.status = JobChainStatus.COMPLETED;
		n.jobChains.add(j);
		
		j = new JobChain();
		j.name = "JOBCHAIN_1A_02";
		j.status = JobChainStatus.COMPLETED;
		n.jobChains.add(j);
		
		n = new DuNode();
		n.name = "NODE1B";
		u.nodes.add(n);
		
		j = new JobChain();
		j.name = "JOBCHAIN_1B_01";
		j.status = JobChainStatus.ABORTED;
		n.jobChains.add(j);
		
		u = new UVMS();
		u.hostname = "TEST_SERVER2";
		u.user = "testuser";
		u.password = "pwd";
		u.webUrl = "http://testserver1.domain.com/web_console";
		conf.uvmsList.add(u);
		
		n = new DuNode();
		n.name = "NODE2A";
		u.nodes.add(n);
		
		n = new DuNode();
		n.name = "NODE2B";
		u.nodes.add(n);
		
		n = new DuNode();
		n.name = "NODE2C";
		u.nodes.add(n);
		
		u = new UVMS();
		u.hostname = "TEST_SERVER3";
		u.user = "testuser";
		u.password = "pwd";
		u.webUrl = "http://testserver1.domain.com/web_console";
		conf.uvmsList.add(u);
		
		n = new DuNode();
		n.name = "NODE3A";
		u.nodes.add(n);
		
		j = new JobChain();
		j.name = "JOBCHAIN_3A_01";
		j.status = JobChainStatus.RUNNING;
		n.jobChains.add(j);
		
		conf.refreshInterval = 30;
		
		return conf; */
	
}
