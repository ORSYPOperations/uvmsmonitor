package com.orsyp.tools.uvmsmonitor;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Configuration implements Serializable {
	private static final long serialVersionUID = -3837994101832823846L;

	public static final String SERIALIZED_FILE = "uvmslist.ser";

	public ArrayList<UVMS> uvmsList = new ArrayList<UVMS>();

	public int refreshInterval = 60; // in seconds

	public void save() {
		try {
			OutputStream buffer = new BufferedOutputStream(new FileOutputStream(Configuration.SERIALIZED_FILE));
			ObjectOutput output = new ObjectOutputStream(buffer);
			try {
				output.writeObject(this);
			} finally {
				output.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
