package com.orsyp.tools.uvmsmonitor;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class SaveRefreshServlet
 */
public class SaveRefreshServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Configuration c = ConfigFactory.getConfig();
		c.refreshInterval = Integer.parseInt(request.getParameter("refresh"));
		c.save();
		
		response.sendRedirect("config.jsp");
	}

}
