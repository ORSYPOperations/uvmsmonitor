package com.orsyp.tools.uvmsmonitor;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DeleteUVMSServlet
 */
public class DeleteUVMSServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Configuration c = ConfigFactory.getConfig();
		
		String delName = request.getParameter("name");
		for (int i = c.uvmsList.size()-1; i >= 0; i--)
			if (c.uvmsList.get(i).hostname.equals(delName))
				c.uvmsList.remove(i);
		c.save();
		
		response.sendRedirect("config.jsp");
	}

}
