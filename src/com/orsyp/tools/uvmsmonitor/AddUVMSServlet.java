package com.orsyp.tools.uvmsmonitor;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddUVMSServlet
 */
public class AddUVMSServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Configuration c = ConfigFactory.getConfig();
		
		String pName = request.getParameter("name");
		String user = request.getParameter("user");
		String pwd = request.getParameter("pwd");
		String web = request.getParameter("web");
		
		for (int i = c.uvmsList.size()-1; i >= 0; i--)
			if (c.uvmsList.get(i).hostname.equals(pName))
				c.uvmsList.remove(i);
		
		UVMS u = new UVMS();
		u.hostname = pName;
		u.password = pwd;
		u.user = user;
		u.webUrl = web;
		
		c.uvmsList.add(u);
		
		Collections.sort(c.uvmsList, new Comparator<UVMS>() {
											@Override
											public int compare(UVMS u1, UVMS u2) {
												return u1.hostname.compareTo(u2.hostname);
											}
										});
		c.save();
		
		response.sendRedirect("config.jsp");
	}

}
