package com.orsyp.tools.uvmsmonitor;

import java.util.ArrayList;
import com.orsyp.Area;
import com.orsyp.Environment;
import com.orsyp.Identity;
import com.orsyp.SyntaxException;
import com.orsyp.UniverseException;
import com.orsyp.api.Client;
import com.orsyp.api.Context;
import com.orsyp.api.Product;
import com.orsyp.api.bv.BvmFilter;
import com.orsyp.api.bv.BvmItem;
import com.orsyp.api.bv.BvmList;
import com.orsyp.api.bv.BvmStatus;
import com.orsyp.api.central.UniCentral;
import com.orsyp.api.security.Operation;
import com.orsyp.central.jpa.jpo.NodeInfoEntity;
import com.orsyp.comm.client.ClientServiceLocator;
import com.orsyp.owls.impl.bv.OwlsBvmListImpl;
import com.orsyp.std.ClientConnectionManager;
import com.orsyp.std.MultiCentralConnectionFactory;
import com.orsyp.std.central.UniCentralStdImpl;

public class UVMSApi {
	
	private UniCentral central = null;
	private UVMS uvms = null;
	

	public ArrayList<JobChain> getJobChainList(DuNode node) {
		ArrayList<JobChain> jobList = new ArrayList<JobChain>();		
	    
        try {
        	Context context = makeContext(node.name, node.company, central, uvms);
    		BvmList list = new BvmList(context, new BvmFilter());
    	    list.setImpl(new OwlsBvmListImpl());
    	    list.setContext(context);	  
			list.extract(Operation.DISPLAY);
			for (int i = 0; i < list.getCount(); i++) {
				BvmItem item = list.get(i);
				
				if (item.getName().toUpperCase().endsWith(".JC")) {				
					JobChain j = new JobChain();
					j.name = item.getName();
					BvmStatus status = item.getStatus();
					j.status = status.getStatus();
					j.color = "#DDDDDD";
					if (status.getColor()!=null) {
						String rgb = Integer.toHexString(status.getColor().getRGB());
						rgb = rgb.substring(2, rgb.length());
						j.color = "#" + rgb;
					}
					jobList.add(j);
				}
			}
			node.connected = true;
		} catch (UniverseException e) {
			System.out.println("Connection error, node " + node.name);
			node.connected = false;
		}
		
        return jobList;
	}
	
	public ArrayList<DuNode> getNodeList(UVMS u){
		uvms=u;
		ArrayList<DuNode> nodeList = new ArrayList<DuNode>();
		try {
			central = getUVMSConnection(uvms);			
			NodeInfoEntity[] nnes = ClientServiceLocator.getNodeInfoService().getAllNodeInfoFromCache(-1, null);

			for (NodeInfoEntity nne : nnes)
				if (nne.getProductCode().equals("DUN")){
					DuNode n = new DuNode();
					n.name = nne.getNodeName();
					n.company = nne.getCompany();
					n.connected = nne.getNodeStatusAreaX()==1; 
					nodeList.add(n);
				}
			
			u.connectionError =false;
		} catch (Exception e) {
			u.connectionError = true;
			e.printStackTrace();
		}
		return nodeList;
	}
	
	private UniCentral getUVMSConnection(UVMS uvms) throws SyntaxException {
		String host = uvms.hostname;
		int port = 4184;
		if (host.contains(":")) {
			String[] tk = host.split(":");
			port = Integer.parseInt(tk[tk.length-1]);
			host = host.replace(":"+tk[tk.length-1], "");
		}
		
		UniCentral cent = new UniCentral(host, port);
		cent.setImplementation(new UniCentralStdImpl(cent));
		
		Context ctx = new Context(new Environment("UJCENT",host), new Client(new Identity(uvms.user, uvms.password, host, "")));
		ctx.setProduct(com.orsyp.api.Product.UNICENTRAL);
		ctx.setUnijobCentral(cent);
		ClientServiceLocator.setContext(ctx);

		try {
			cent.login(uvms.user, uvms.password);
			if (ClientConnectionManager.getDefaultFactory() == null) {
	            ClientConnectionManager.setDefaultFactory(MultiCentralConnectionFactory.getInstance());
	        }
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return cent;
	}
	
	private Context makeContext(String node, String company, UniCentral central, UVMS uvms) throws SyntaxException {
		Context ctx = null;
		Client client = new Client(new Identity(uvms.user, "", node, ""));
		ctx = new Context(new Environment(company, node, Area.Exploitation), client, central);
		ctx.setProduct(Product.OWLS);
		return ctx;
	}
	
}
