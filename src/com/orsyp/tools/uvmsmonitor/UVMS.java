package com.orsyp.tools.uvmsmonitor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class UVMS implements Serializable{
	private static final long serialVersionUID = -6815691112370805138L;
	
	public String hostname;
	public String user;
	public String password;
	public String webUrl;
	public boolean connectionError = false;
	transient private UVMSApi api;
	
	transient public ArrayList<DuNode> nodes = new ArrayList<DuNode>();
	
	public void loadData() {
		api = new UVMSApi();
		ArrayList<DuNode> nodeList = loadNodes();
		for (DuNode node : nodeList)
			loadNodeData(node);
		nodes = nodeList;
	}
	
	private ArrayList<DuNode> loadNodes() {
		ArrayList<DuNode> nodeList =  api.getNodeList(this);
		
		Collections.sort(nodeList, new Comparator<DuNode>() {
			@Override
			public int compare(DuNode n1, DuNode n2) {
				return n1.name.compareTo(n2.name);
			}
		});		
		return nodeList;
	}

	private void loadNodeData(DuNode node) {
		ArrayList<JobChain> jobs =  api.getJobChainList(node);
		
		Collections.sort(jobs, new Comparator<JobChain>() {
			@Override
			public int compare(JobChain j1, JobChain j2) {
				return j1.name.compareTo(j2.name);
			}
		});
		
		node.jobChains = jobs;
	}
}
